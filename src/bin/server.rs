use std::env;

use std::net::SocketAddr;
use std::sync::{Arc, Mutex};
use log::info;
use tokio;

type SharedConfig = Arc<Mutex<config::Config>>;

mod config {
    use anyhow::Result;
    use std::sync::Mutex;
    use std::{fs, sync::Arc, thread, time::Duration};

    use serde::{Deserialize, Serialize};

    use super::SharedConfig;

    #[derive(Clone, Deserialize, Serialize)]
    pub struct Config {
        pub reload_config_after_sec: u64,
        pub host: String,
        pub log_config_file: String,
        pub files: FilesConfig,
        pub database: DatabaseConfig,
        pub iiif: IIIFConfig,
        #[serde(skip)]
        config_path: String,
    }

    impl Config {
        pub fn load_from_file(path: &str) -> Result<Self> {
            let file_content = fs::read_to_string(path)?;
            let mut config: Self = toml::from_str(&file_content)?;
            config.config_path = path.to_owned();
            Ok(config)
        }

        fn reload_from_file(&self) -> Result<Self> {
            Config::load_from_file(&self.config_path)
        }

        pub fn reload_continuously(self) -> SharedConfig {
            let duration = self.reload_config_after_sec;
            let handler = Arc::new(Mutex::new(self));
            let config_write = Arc::clone(&handler);
            thread::spawn(move || {
                let mut sleep_for = Duration::new(duration, 0);
                loop {
                    thread::sleep(sleep_for);
                    let mut data = config_write.lock().expect("Configuration mutex lock failed");
                    *data = data.reload_from_file().expect("Reloading configuration failed");
                    sleep_for = Duration::new(data.reload_config_after_sec, 0);
                }
            });
            handler
        }
    }

    #[derive(Clone, Deserialize, Serialize)]
    pub struct FilesConfig {
        pub path: String,
        pub path_derivatives: String,
    }

    #[derive(Clone, Deserialize, Serialize)]
    pub struct DatabaseConfig {}

    #[derive(Clone, Deserialize, Serialize)]
    pub struct IIIFConfig {}
}

mod handlers {
    use std::path::{Path, PathBuf};

    use super::SharedConfig;
    use i3f4r::iiif::Region;
    use tokio::fs::{self, File};
    use tokio::io::AsyncWriteExt;
    use warp::hyper::StatusCode;
    use warp::Buf;

    pub async fn iiif(
        id: String,
        region: String,
        size: String,
        rotation: String,
        quality_format: String,
    ) -> Result<impl warp::Reply, warp::Rejection> {
        let region = Region::parse(&region).unwrap();
        Ok(format!(
            "id: {}; size: {}; rotation: {}; quality_format: {}",
            id, size, rotation, quality_format
        ))
    }

    pub async fn import_image(
        id: String,
        mut image_body: impl Buf,
        config: SharedConfig,
    ) -> Result<impl warp::Reply, warp::Rejection> {
        let file_path = build_file_path(id, config.clone());
        let f = File::create(file_path).await;
        if f.is_err() {
            return Ok(StatusCode::INTERNAL_SERVER_ERROR);
        }
        if f.unwrap().write_all_buf(&mut image_body).await.is_err() {
            return Ok(StatusCode::INTERNAL_SERVER_ERROR);
        }
        Ok(StatusCode::OK)
    }

    pub async fn delete_image(id: String, config: SharedConfig) -> Result<impl warp::Reply, warp::Rejection> {
        let file_path = build_file_path(id, config.clone());
        if fs::remove_file(file_path).await.is_err() {
            Ok(StatusCode::INTERNAL_SERVER_ERROR)
        } else {
            Ok(StatusCode::OK)
        }
    }

    pub async fn get_image(id: String, config: SharedConfig) -> Result<impl warp::Reply, warp::Rejection> {
        let file_path = build_file_path(id, config.clone());
        let f = fs::read(file_path).await.unwrap();
        let body = warp::hyper::Body::from(f);
        warp::http::Response::builder().body(body).map_err(|_| warp::reject())
    }

    fn build_file_path(id: String, config: SharedConfig) -> PathBuf {
        let config = config.lock().expect("Mutex locking failed");
        let file_dir = config.files.path.to_owned();
        Path::new(&file_dir).join(&id)
    }
}

mod filters {
    use super::SharedConfig;
    use crate::handlers;
    use warp::Filter;

    pub fn routes(config: SharedConfig) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        health()
            .or(iiif())
            .or(post_image(config.clone()))
            .or(delete_image(config.clone()))
            .or(get_image(config.clone()))
    }

    fn health() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path("health").map(|| "ok".to_string())
    }

    fn iiif() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::get()
            .and(warp::path!("iiif" / String / String / String / String / String))
            .and_then(handlers::iiif)
    }

    fn post_image(config: SharedConfig) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::post()
        .and(warp::path!("image" / String))
        // TODO: This goes to settings
        .and(warp::body::content_length_limit(1024 * 1024 * 100))
        .and(warp::body::aggregate())
        .and(with_config(config))
        .and_then(handlers::import_image)
    }

    fn delete_image(config: SharedConfig) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::delete()
            .and(warp::path!("image" / String))
            .and(with_config(config))
            .and_then(handlers::delete_image)
    }

    fn get_image(config: SharedConfig) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::get()
            .and(warp::path!("image" / String))
            .and(with_config(config))
            .and_then(handlers::get_image)
    }

    fn with_config(
        config: SharedConfig,
    ) -> impl Filter<Extract = (SharedConfig,), Error = std::convert::Infallible> + Clone {
        warp::any().map(move || config.clone())
    }
}

#[tokio::main]
async fn main() {
    let config_path = env::args().nth(1).expect("No configuration path defined!");
    let config = config::Config::load_from_file(&config_path).expect("Reading of configuration file failed");
    let _log_handle = log4rs::init_file(&config.log_config_file, Default::default()).expect("Reading of log configuration file failed!");
    let config = config.reload_continuously();
    info!("Application starting up");

    let socket_addr: SocketAddr = config
        .lock()
        .expect("Mutex locking failed")
        .host
        .parse()
        .expect("Unable to parse host address");
    warp::serve(filters::routes(config)).run(socket_addr).await;
}
