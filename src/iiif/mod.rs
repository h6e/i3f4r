use anyhow::{bail, Result};
use lazy_static::lazy_static;
use regex::Regex;

#[derive(Copy, Clone)]
pub enum Region {
    Full,
    Square,
    Pixels(u16, u16, u16, u16),
    Percentage(u16, u16, u16, u16),
}

impl Region {
    #[allow(non_upper_case_globals)]
    pub fn parse(region: &str) -> Result<Region> {
        use Region::*;

        lazy_static! {
            static ref PixelRe: Regex = Regex::new(r"^(\d+),(\d+),(\d+),(\d+)$").unwrap();
            static ref PctRe: Regex = Regex::new(r"^pct:(\d+),(\d+),(\d+),(\d+)$").unwrap();
        }
        match region {
            "full" => Ok(Full),
            "square" => Ok(Square),
            r if PixelRe.is_match(r) => {
                let cap = PixelRe.captures(r).unwrap();
                let x: u16 = cap[0].parse()?;
                let y: u16 = cap[1].parse()?;
                let w: u16 = cap[2].parse()?;
                let h: u16 = cap[3].parse()?;
                Ok(Pixels(x, y, w, h))
            }
            r if PctRe.is_match(r) => {
                let cap = PctRe.captures(r).unwrap();
                let x: u16 = cap[0].parse()?;
                let y: u16 = cap[1].parse()?;
                let w: u16 = cap[2].parse()?;
                let h: u16 = cap[3].parse()?;
                Ok(Percentage(x, y, w, h))
            }
            _ => bail!("{} is an invalid format for region", region),
        }
    }
}

#[derive(Copy, Clone)]
pub enum Size {
    Max,
    MaxUpscaled,
    Width(u16),
    WidthUpscaled(u16),
    Height(u16),
    HeightUpscaled(u16),
    Relative(u8),
    RelativeUpscaled(u16),
    WidthHeight(u16, u16),
    WidthHeightUpscaled(u16, u16),
    WidthHeightLockedRatio(u16, u16),
    WidthHeightLockedRatioUpscaled(u16, u16),
}

impl Size {
    #[allow(non_upper_case_globals)]
    pub fn parse(size: &str) -> Result<Size> {
        use Size::*;
        lazy_static! {
            static ref WidthRe: Regex = Regex::new(r"^(\d+),$").unwrap();
            static ref WidthUpscaledRe: Regex = Regex::new(r"^\^(\d+),$").unwrap();
            static ref HeightRe: Regex = Regex::new(r"^,(\d+)$").unwrap();
            static ref HeightUpscaledRe: Regex = Regex::new(r"^\^,(\d+)$").unwrap();
            static ref RelativeRe: Regex = Regex::new(r"^pct:(\d+)$").unwrap();
            static ref RelativeUpscaledRe: Regex = Regex::new(r"^\^pct:(\d+)$").unwrap();
            static ref WidthHeightRe: Regex = Regex::new(r"^(\d+),(\d+)$").unwrap();
            static ref WidthHeightUpscaledRe: Regex = Regex::new(r"^\^(\d+),(\d+)$").unwrap();
            static ref WidthHeightLockedRatioRe: Regex = Regex::new(r"^!(\d+),(\d+)$").unwrap();
            static ref WidthHeightLockedRatioUpscaledRe: Regex =
                Regex::new(r"^\^!(\d+),(\d+)$").unwrap();
        }

        match size {
            "max" => Ok(Max),
            "^max" => Ok(MaxUpscaled),
            s if WidthRe.is_match(s) => {
                let cap = WidthRe.captures(s).unwrap();
                let w: u16 = cap[0].parse()?;
                Ok(Width(w))
            }
            s if WidthUpscaledRe.is_match(s) => {
                let cap = WidthUpscaledRe.captures(s).unwrap();
                let w: u16 = cap[0].parse()?;
                Ok(WidthUpscaled(w))
            }
            s if HeightRe.is_match(s) => {
                let cap = HeightRe.captures(s).unwrap();
                let h: u16 = cap[0].parse()?;
                Ok(Height(h))
            }
            s if HeightUpscaledRe.is_match(s) => {
                let cap = HeightUpscaledRe.captures(s).unwrap();
                let h: u16 = cap[0].parse()?;
                Ok(HeightUpscaled(h))
            }
            s if RelativeRe.is_match(s) => {
                let cap = RelativeRe.captures(s).unwrap();
                let p: u8 = cap[0].parse()?;
                Ok(Relative(p))
            }
            s if RelativeUpscaledRe.is_match(s) => {
                let cap = HeightUpscaledRe.captures(s).unwrap();
                let p: u16 = cap[0].parse()?;
                Ok(RelativeUpscaled(p))
            }
            s if WidthHeightRe.is_match(s) => {
                let cap = WidthHeightRe.captures(s).unwrap();
                let w: u16 = cap[0].parse()?;
                let h: u16 = cap[1].parse()?;
                Ok(WidthHeight(w, h))
            }
            s if WidthHeightUpscaledRe.is_match(s) => {
                let cap = WidthHeightUpscaledRe.captures(s).unwrap();
                let w: u16 = cap[0].parse()?;
                let h: u16 = cap[1].parse()?;
                Ok(WidthHeightUpscaled(w, h))
            }
            s if WidthHeightLockedRatioRe.is_match(s) => {
                let cap = WidthHeightLockedRatioRe.captures(s).unwrap();
                let w: u16 = cap[0].parse()?;
                let h: u16 = cap[1].parse()?;
                Ok(WidthHeightLockedRatio(w, h))
            }
            s if WidthHeightLockedRatioUpscaledRe.is_match(s) => {
                let cap = WidthHeightLockedRatioUpscaledRe.captures(s).unwrap();
                let w: u16 = cap[0].parse()?;
                let h: u16 = cap[1].parse()?;
                Ok(WidthHeightLockedRatioUpscaled(w, h))
            }
            _ => bail!("{} is an invalid format for size", size),
        }
    }
}

#[derive(Copy, Clone)]
pub enum Rotation {
    Clockwise(u16),
    Mirrored(u16),
}

impl Rotation {
    #[allow(non_upper_case_globals)]
    pub fn parse(rotation: &str) -> Result<Rotation> {
        use Rotation::*;
        lazy_static! {
            static ref ClockwiseRe: Regex = Regex::new(r"^(\d{1,3})$").unwrap();
            static ref MirroredRe: Regex = Regex::new(r"^!(\d{1,3})$").unwrap();
        }
        match rotation {
            r if ClockwiseRe.is_match(r) => {
                let cap = ClockwiseRe.captures(r).unwrap();
                let d = cap[0].parse()?;
                if d > 360 {
                    bail!("rotation must be equal or less than 360°")
                } else {
                    Ok(Clockwise(d))
                }
            }
            r if MirroredRe.is_match(r) => {
                let cap = MirroredRe.captures(r).unwrap();
                let d = cap[0].parse()?;
                if d > 360 {
                    bail!("rotation must be equal or less than 360°")
                } else {
                    Ok(Mirrored(d))
                }
            }
            _ => bail!("{} is an invalid format for rotation", rotation),
        }
    }
}

#[derive(Copy, Clone)]
pub enum Quality {
    Color,
    Gray,
    Bitonal,
    // TODO: Remove default (is one of the other variants, depending on settings)
    Default,
}

impl Quality {
    pub fn parse(quality: &str) -> Result<Quality> {
        use Quality::*;
        match quality {
            "color" => Ok(Color),
            "gray" => Ok(Gray),
            "bitonal" => Ok(Bitonal),
            "default" => Ok(Default),
            _ => bail!("{} is an unknown quality", quality),
        }
    }
}

#[derive(Copy, Clone)]
pub enum Format {
    Jpg,
    Tif,
    Png,
    Gif,
    Jp2,
    Pdf,
    WebP,
}

impl Format {
    pub fn parse(format: &str) -> Result<Format> {
        use Format::*;
        match format {
            "jpg" => Ok(Jpg),
            "tif" => Ok(Tif),
            "png" => Ok(Png),
            "gif" => Ok(Gif),
            "jp2" => Ok(Jp2),
            "pdf" => Ok(Pdf),
            "webp" => Ok(WebP),
            _ => bail!("{} is an unknown format", format),
        }
    }
}
